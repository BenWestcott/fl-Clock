#ifndef tabmanager_h__
#define tabmanager_h__

#include "ClockApp.h++"
#include <FL/Fl_Window.H>

class TabManager {
	public:
		static void switchAlarm(void* clockApp);
		static void switchClock(void* clockApp);
		static void switchTimer(void* clockApp);
		static void switchStopwatch(void* clocApp);
		static void switchSettings(void* clockApp);
};

#endif