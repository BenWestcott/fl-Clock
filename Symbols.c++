#include "Symbols.h++"
#include <FL/fl_draw.H>

void Symbols::add() {
	fl_add_symbol("alarm", Symbols::drawAlarm, 1);
	fl_add_symbol("clock", Symbols::drawClock, 1);
	fl_add_symbol("timer", Symbols::drawTimer, 1);
	fl_add_symbol("stopwatch", Symbols::drawStopwatch, 1);
	fl_add_symbol("settings", Symbols::drawSettings, 1);
}

void Symbols::drawAlarm(Fl_Color c) {
	// Body
	fl_begin_complex_polygon();
	fl_arc(0, 0, 0.7, 0, 360);
	fl_gap();
	fl_arc(0, 0, 0.6, 360, 0);
	fl_end_complex_polygon();
	// Hands
	fl_begin_polygon();
	fl_vertex(-0.05, 0.05);
	fl_vertex(-0.05, -0.5);
	fl_vertex(0.05, -0.5);
	fl_vertex(0.05, -0.05);
	fl_vertex(0.3, -0.05);
	fl_vertex(0.3, 0.05);
	fl_end_polygon();
	// Left bell
	fl_begin_polygon();
	fl_arc(-0.3, -0.3, 0.5, 90, 180);
	fl_end_polygon();
	// Right bell
	fl_begin_polygon();
	fl_arc(0.3, -0.3, 0.5, 0, 90);
	fl_end_polygon();
}

void Symbols::drawClock(Fl_Color c) {
	// Body
	fl_begin_complex_polygon();
	fl_arc(0, 0, 0.7, 0, 360);
	fl_gap();
	fl_arc(0, 0, 0.6, 360, 0);
	fl_end_complex_polygon();
	// Hands
	fl_begin_polygon();
	fl_vertex(-0.05, 0.05);
	fl_vertex(-0.05, -0.5);
	fl_vertex(0.05, -0.5);
	fl_vertex(0.05, -0.05);
	fl_vertex(0.3, -0.05);
	fl_vertex(0.3, 0.05);
	fl_end_polygon();
}

void Symbols::drawTimer(Fl_Color c) {
	
}

void Symbols::drawStopwatch(Fl_Color c) {
	
}

void Symbols::drawSettings(Fl_Color c) {
	
}
