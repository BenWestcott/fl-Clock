#ifndef symbols_h__
#define symbols_h__

#include <FL/Enumerations.H>

class Symbols {
	public:
		static void add();
	private:
		static void drawAlarm(Fl_Color c);
		static void drawClock(Fl_Color c);
		static void drawTimer(Fl_Color c);
		static void drawStopwatch(Fl_Color c);
		static void drawSettings(Fl_Color c);
};
#endif