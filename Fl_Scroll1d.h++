#ifndef fl_scroll1d_h__
#define fl_scroll1d_h__

#include <FL/Fl.H>
#include <FL/Fl_Scroll.H>

class Fl_Scroll1d : public Fl_Scroll {
	public:
	Fl_Scroll1d(int x, int y, int w, int h, const char* label = 0);
	void resize(int x, int y, int w, int h);
	void add(Fl_Widget& w);
	void add(Fl_Widget* w) { this->add(*w); }
	void add_resizable(Fl_Widget& w) { this->add(w); };
	private:
	Fl_Widget* first;
	Fl_Widget* last;
};

#endif
