// generated by Fast Light User Interface Designer (fluid) version 1.0305

#ifndef AlarmEntry_h__
#define AlarmEntry_h__
#include <FL/Fl.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Value_Input.H>
#include <FL/Fl_Light_Button.H>
#include <FL/Fl_Button.H>

class AlarmEntry : public Fl_Group {
public:
  AlarmEntry(int X, int Y, int W, int H, const char *L = 0);
  Fl_Group *AlarmEntryRow2;
  Fl_Input *AlarmEntryLabel;
  Fl_Group *AlarmEntryRow1;
  Fl_Value_Input *AlarmEntryHour;
  Fl_Value_Input *AlarmEntryMinute;
  Fl_Light_Button *AlarmEntryEnable;
  Fl_Light_Button *AarmEntryVibrate;
  Fl_Group *AlarmEntryRow3;
  Fl_Light_Button *AlarmEntryRepeat;
  Fl_Light_Button *AlarmEntryMonday;
  Fl_Light_Button *AlarmEntryTueseday;
  Fl_Light_Button *AlarmEntryWednesday;
  Fl_Light_Button *AlarmEntryThursday;
  Fl_Light_Button *AlarmEntryFriday;
  Fl_Light_Button *AlarmEntrySaturday;
  Fl_Light_Button *AlarmEntrySunday;
};
#endif
