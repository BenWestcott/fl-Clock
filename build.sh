#!/bin/sh
CC="g++"
FILES="AlarmEntry.c++ ClockApp.c++ Symbols.c++ TabManager.c++ Fl_Scroll1d.c++"
LIBS="fl-widgets/Scroll_1d.c++"
FLTK_FLAGS=$(fltk-config --cxxflags --ldflags)

$CC Main.c++ $FILES $LIBS $FLTK_FLAGS
