#include "TabManager.h++"

void TabManager::switchAlarm(void* clockApp) {
	ClockUI* ui = (ClockUI*)clockApp;
	ui->hideApplets();
	ui->AppletAlarm->show();
}

void TabManager::switchClock(void* clockApp) {
	ClockUI* ui = (ClockUI*)clockApp;
	ui->hideApplets();
	ui->AppletClock->show();
}

void TabManager::switchTimer(void* clockApp) {
	ClockUI* ui = (ClockUI*)clockApp;
	ui->hideApplets();
	ui->AppletTimer->show();
}

void TabManager::switchStopwatch(void* clockApp) {
	ClockUI* ui = (ClockUI*)clockApp;
	ui->hideApplets();
	ui->AppletStopwatch->show();
}

void TabManager::switchSettings(void* clockApp) {
	ClockUI* ui = (ClockUI*)clockApp;
	ui->hideApplets();
	ui->AppletSettings->show();
}
