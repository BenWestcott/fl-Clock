#include "Fl_Scroll1d.h++"

Fl_Scroll1d::Fl_Scroll1d(int x, int y, int w, int h, const char* label) : Fl_Scroll(x, y, w, h, label) {
	
}

void Fl_Scroll1d::add(Fl_Widget& w) {
	int y = this->last->y() + this->last->h();
	w.pos(w.x(), y);
	Fl_Scroll::add(w);
	printf("Test\n");
}

void Fl_Scroll1d::resize(int x, int y, int w, int h) {
	Fl_Scroll::resize(x, y, w, h);
	for (Fl_Widget* const* i = this->array(); i != nullptr; ++i) {
		Fl_Widget* item = *i;
		item->resize(item->x(), item->y(), w, item->h()); // Only resize width
	}
}